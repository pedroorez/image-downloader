import boto3
import json
import requests
import asyncio
import aiohttp
import threading
import memcache
import hashlib

from PIL import Image
from io import BytesIO


# Constants
# MEMCACHED_URL = "memcached"
QUEUE_NAME = "process.fifo"
BUCKET_NAME = "down.test.com"
REGION = 'eu-west-1'
WORKERS_NUM = 4
WAIT_TIME_SECONDS = 20
CONCURENT_DOWNLOADS = 30
MAX_NUM_MSG = 1
DISABLE_MD5_CHECK = True


# Setup
s3 = boto3.client("s3")
sqs = boto3.resource("sqs", region_name=REGION)
queue = sqs.get_queue_by_name(QueueName=QUEUE_NAME)
concurrency_sem = asyncio.Semaphore(CONCURENT_DOWNLOADS)
loop = asyncio.get_event_loop()

if not DISABLE_MD5_CHECK:
	mc = memcache.Client([MEMCACHED_URL])


# Worker
def consume_queue():
	for message in queue.receive_messages(WaitTimeSeconds=WAIT_TIME_SECONDS, MaxNumberOfMessages=MAX_NUM_MSG):
		imovel = json.loads(message.body)

		if DISABLE_MD5_CHECK or mc.add(hashlib.md5(imovel.encode('utf-8')).hexdigest(), "Working on!"):
			consume_message(imovel)

			# Delete Message Completing Request
			message.delete()
		else:
			print ("> MESSAGE ALREADY PROCESSED")


# Process message
def consume_message(imovel):
	try:	
		imovel_request = asyncio.wait([process_and_save_photo(photo["URLArquivo"][0], photo["NomeArquivo"][0], imovel["id"]) for photo in imovel["photos"]])
		loop.run_until_complete(imovel_request)

	except Exception as e:
		print ("> ERROR PROCESSING MESSAGE | ID_IMOVEL: {}".format(imovel["id"]))
		print (e)


# Download and process a single photo
@asyncio.coroutine
def process_and_save_photo(url, filename, id):
	# Get Async Token
	yield from concurrency_sem.acquire()

	print ("> Starting Photo Saving {} / {}".format(filename, id))

	try:
		# Get Original Photo
		with aiohttp.ClientSession() as session:
			response = yield from session.request('GET', url)
			orig_photo = yield from response.read()

		# Process Photo
		processed_photo = process_photo(orig_photo)

		# TODO: Try to transform save_to_s3 as a asyncio process
		# Not a big of issue. Inside a EC2 the transfer to S3 should
		# be near to instantaneous	

		# Save Original Photo
		save_to_s3(id, "original_photos", filename, orig_photo)

		# Save Original Processed
		save_to_s3(id, "resized_photos", filename, processed_photo)
		
		print ("> Finished Photo {} Saving".format(filename))

		# Release Async Token
		concurrency_sem.release()
	except Exception as e:
		print ("> ERROR DOWNLOADING PHOTO {} / ID {}".format(filename, id))
		print (e)


# Save Photo to S3
def save_to_s3(id, folder_name, filename, data):
	key = "imoveis/{}/{}/{}".format(id, folder_name, filename)
	s3.put_object(Bucket=BUCKET_NAME, Key=key, Body=data, ACL="public-read", ContentType="image/jpeg")


# Process Image
def process_photo(image_buffer):
	image = Image.open(BytesIO(image_buffer))
	final_image_buffer = BytesIO()

	image.thumbnail([300, 300])
	image.save(final_image_buffer, 'JPEG')
	final_image_buffer.seek(0)

	return final_image_buffer


# # Start Worker
def worker(worker_id):
	while True:
		print ("> WORKER {} | Start Consuming".format(worker_id))
		try:
			consume_queue()
		except:
			pass


# Create Workers
for i in range(WORKERS_NUM):
    thread = threading.Thread(target=worker, args=(i,))
    thread.start()
