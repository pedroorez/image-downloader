'use strict';


// Constants
const QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/240639734538/process.fifo";
const REGION = "eu-west-1"


// Libs
const Promise = require('bluebird');
const parseString = Promise.promisify(require('xml2js').parseString);


// AWS Setup
const AWS = require('aws-sdk');
AWS.config.setPromisesDependency(Promise); // Load bluebird as Promise Lib
const S3 = new AWS.S3();
const SQS = new AWS.SQS({ apiVersion: '2012-11-05', region: REGION });


// SQS Message Creator
// Creates a Message on the defined QUEUE_URL
const createSQSItem = (item) => {
	let MessageBody = {
		"id": item.CodigoImovel[0],
		"photos": item.Fotos[0].Foto,
	}

	let params = {
	 MessageGroupId: 'image_process',
	 MessageBody: JSON.stringify(MessageBody),
	 QueueUrl: QUEUE_URL,
	}

	return SQS.sendMessage(params).promise()
}


// LAMBDA HANDLER
exports.handler = (event, context, callback) => 
	// Convert XML to JSON
	parseString(event.body)
		.then((converted_xml) => {
			let imoveisJson = converted_xml.Imoveis.Imovel
			return Promise.all(imoveisJson.map((item) => createSQSItem(item)))
		})
		.then((result) => {
			callback(null, { "statusCode": 200, "body": '{"status": "XML imported!"}' })
		})
		.catch((err) => {
			console.error(err)
			callback(null, { "statusCode": 500, "body": '{"status": "Something went wrong :/}' })
		});

	
