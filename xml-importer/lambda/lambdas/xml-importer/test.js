'use strict'

const fs = require('fs');
const lambda = require('./index.js')


// LAMBDA EVENT MOCK
var event = {
	body: fs.readFileSync('imoveis-mock.xml'),
}


// LAMBDA RUN
lambda.handler(event, {}, console.log)