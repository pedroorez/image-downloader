#!/bin/bash

# Required Constants
export REGION=eu-west-1
export SQS_QUEUE=0
export SQS_QUEUE_NAME=0
export BUCKET_NAME=0

# Install EBCLI
pip install awsebcli --upgrade --user

# Deploy Image-Processor
cd image-processor 
eb create image-processor

# Go Back to Root
cd ..

# Deploy XML-Importer
# Install Myrmex
npm install myrmex -g

# Deploy IAM/Lambda/ApiGateway for xml-importer
cd xml-importer
myrmex deploy-apis xml-importer --region eu-west-1 --environment DEV --stage v0 --deploy-lambdas all --alias ""