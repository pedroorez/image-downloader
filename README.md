# XML Image Downloader

This project have the object of receiving a XML file and download every group of images for each item defined.

This project is devided in two parts: a Lambda function to parse and create SQS messages and a SQS consumer.

## Requirements

To run this project you will need a FIFO SQS Queue created open to eveyone on the account.

## Easy deploy

Just run the `deploy.sh` script on the root of the project. 

You must have valid AWS Credentials and define the variables on the top of the script before running.

## Lambda XML Importer

This Importer is made with Api Gateway and Lambda using Myrmex for deployment.

The core of the solution is located on `lambda/lambdas/xml-importer/index.js`. It implements a lambda function that receive the XML, parse it and create a SQS Message for each item to process.

The rest of the folders and files are configuration for Myrmex to create the IAM roles, Apigateway, lambda function etc...

### Credentials

You must have valid AWS Credentials on ~/.aws folder for deployment.

For deployment you will need IAM, ApiGateway and Lambda permissions.

For testing locally you dont need any permission.

### Testing 

You must also define the REGION and QUEUE_URL of your SQS queue on `index.js` file, then just run:

```
npm start
```

This will create SQS messages based on the `imoveis-mock.xml` file.

### Deployment

For deployment you only need to run:

'''
myrmex deploy-apis xml-importer --region eu-west-1 --environment DEV --stage v0 --deploy-lambdas all --alias ""
'''

This will create the required roles, ApiGateway and Lambda Functions and show the final URL on the screen.

## Image Processor

This SQS Consumer is made with Docker and Python.

It consumes a SQS Queue, process images and save on S3 both original and resized photo.

If this applications runs using a FIFO SQS the flag `DISABLE_MD5_CHECK` can be disabled since FIFO will guarantee that ever message will be deliver only ONCE. If you use SQS Standard Queue or another queue system you must activate this flag to guarantee that each message will be processed only once.

Attention: If you use dont use FIFO SQS QUEUE you must define `MEMCACHED_URL` and disable `DISABLE_MD5_CHECK` constant to validate that every message will be consumed only once.

## Decoupling from AWS Services

This projects is highly dependent of AWS services. But with a few small changes it can be adapted to worker in any infrastructure.

The worker have a function that digest a messages, so to adapt the worker you only need to implement a queue consumer to use this message digest function.

If the queue doenst guarantee that every message will be delivered once you MUST deactive the `DISABLE_MD5_CHECK` variable and add a `MEMCACHED_URL`.

The XML-Importer is very simple and can be re-implemented in any language with no big trouble. If you are too lazy you can import the function and create a node express server that only use the handler function to operate.

### Deployment

To deploy the application on ElasticbeanStalk you just need to run inside `image-procesosr` folder the following command:

You must also install awsebcli using pip before running it.

'''
eb deploy image-processor
'''

## Scalling

This application uses ElasticbeanStalk to manage the Queue Consumer. The horizontal and vertical scalling can be archieved by change the minimal amount of machines. 

Be default the ElasticbeanStalk will automatically scale up to 4 machines if the CPU percentage is too high for a long time but this can be improved by creating a custom trigger to scale up the infrastructure.

Since the nature of the application the size of the machine doesnt have to be too big. A T2.Small is more than enought so horizontal scalling is recomended over the vertical one. 

The XML-Importer uses Lambda and Api-Gateway that auto scale as required.

## Tweaks

The Queue Consuption can be tweaked by changing the constants defined on the begining of the `task.py`. This should be a more impirical depending of the size of images and what kind of processing will be done.

'''
WORKERS_NUM = 4
WAIT_TIME_SECONDS = 20
CONCURENT_DOWNLOADS = 30
MAX_NUM_MSG = 1
'''

## Cost

Lambda and ApiGateway will probably fall under the AWS Free Tier since the amount of request is so small for this API, so it'll be pratically Free.

FIFO SQS have a Free Tier of 1 Million Requests and a 0.50 cents for each Million requests. The final cost of 30 Million requests (1Million ever day) will fall under 15 buck/month.

FIFO SQS Queues are more expensive but with them we dont need a memcache running on a EC2 instance to guarantee that everymessage is delivered only once.

ElasticbeanStalk have a free costing for use but all the components that it uses are charged. Depending on the business need many strategies can be choosen. 

If there is no need to process the imported files on the moment the scalling of the machines could be set to ZERO machines until a certain amount of Items on QUEUE are rechead. 

Independent of the needs all EC2 machines can be SPOT INSTANCES which are way cheaper but they can be terminated anytime depending on the market price. Since the nature of the problem there is no need to have dedicated instances which are more expensive.

With both scaling on demand and spot instances the final COST can be extremely low even if the need to process is high since the recomended scaling is horizontal.

Using Lambda and Starting Workers on demand the IDLE COST of the infrastructure is ZERO.

## Processing Bad Processed Messages

Any messages that are badly processed a certain (configurable) amount of time will be sent to a Dead Letter Queue. This is a normal SQS Queue that can be used to process thoses bad messages. 

A dump of the execution can be saved by a special worker or a email can be sent on the end of the day with the resume of theses failed processed messages

## Improvements

* Adds CloudWatch logging for lambda execution and worker
* Creates Triggers to scale up the application based on the amount of messages on queue
* Improves errors handling on both Worker and Producer
* Tweak the Worker to a specific instance creating more threads, workers, concurrent downloads etc
* many many many many many many many many many many many many more

